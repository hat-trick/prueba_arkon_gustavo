import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router } from "react-router-dom";
import App from "App";
import { Provider } from "react-redux";
import store from "redux/store";
const hist = createBrowserHistory();
ReactDOM.render(
  <Provider store={store}>
    <Router history={hist}>
      <div>
        <App></App>
      </div>
    </Router>
  </Provider>,
  document.getElementById("root")
);
