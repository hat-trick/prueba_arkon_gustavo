export default {
  init: {
    initialized: true
  },
  finished: [],
  tasks: [
    {
      name: "Dormir",
      hours: 1,
      minutes: 20,
      description: "Dormir Mucho",
      seconds: 60,
      finished: false,
      status: "created",
      lapsedHours: 0,
      lapsedMinutes: 0,
      lapsedSeconds: 0
    },
    {
      name: "Comer",
      hours: 1,
      minutes: 0,
      description: "Comer Mucho",
      seconds: 60,
      finished: false,
      status: "created",
      lapsedHours: 0,
      lapsedMinutes: 0,
      lapsedSeconds: 0
    },
    {
      name: "Trabajar",
      hours: 1,
      minutes: 1,
      description: "Trabajar Mucho",
      seconds: 60,
      finished: false,
      status: "created",
      lapsedHours: 0,
      lapsedMinutes: 0,
      lapsedSeconds: 0
    },
    {
      name: "Correr",
      hours: 0,
      minutes: 30,
      description: "Salir a correr",
      seconds: 60,
      finished: false,
      status: "created",
      lapsedHours: 0,
      lapsedMinutes: 0,
      lapsedSeconds: 0
    },
    {
      name: "Pintar Casa",
      hours: 1,
      minutes: 5,
      description: "Pintar la casa de color azul",
      seconds: 60,
      finished: false,
      status: "created",
      lapsedHours: 0,
      lapsedMinutes: 0,
      lapsedSeconds: 0
    },
    {
      name: "Alimentar Mascota",
      hours: 0,
      minutes: 10,
      description: "Alimentar a la mascota",
      seconds: 60,
      finished: false,
      status: "created",
      lapsedHours: 0,
      lapsedMinutes: 0,
      lapsedSeconds: 0
    },
    {
      name: "Compras",
      hours: 1,
      minutes: 39,
      description: "Ir de compras",
      seconds: 60,
      finished: false,
      status: "created",
      lapsedHours: 0,
      lapsedMinutes: 0,
      lapsedSeconds: 0
    },
    {
      name: "Mover Auto",
      hours: 0,
      minutes: 10,
      description: "Mover el auto de lugar",
      seconds: 60,
      finished: false,
      status: "created",
      lapsedHours: 0,
      lapsedMinutes: 0,
      lapsedSeconds: 0
    }
  ]
};
