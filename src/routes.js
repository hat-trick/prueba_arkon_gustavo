import Home from "containers/Home";

const dashboardRoutes = [
  {
    path: "/home",
    component: Home,
    layout: "/admin"
  }
];

export default dashboardRoutes;
