import React from "react";
import { connect } from "react-redux";
import { Grid } from "@material-ui/core";
import Dashboard from "components/Dashboard";

class Tasks extends React.Component {
  state = {
    task: {}
  };

  render() {
    const { tasks } = this.props;

    return (
      <div>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Dashboard tasks={tasks || []} />
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  init: state.init,
  tasks: state.tasks
});

const mapDispatchToProps = dispatch => ({});
export default connect(mapStateToProps, mapDispatchToProps)(Tasks);
