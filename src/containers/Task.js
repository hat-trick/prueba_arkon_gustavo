import React from "react";
import { connect } from "react-redux";

import CreateTask from "components/CreateTask";
import TasksList from "components/TasksList";
import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";

import {
  decrementHours,
  decrementMinutes,
  decrementSeconds,
  pauseTask,
  finishTask,
  createTask,
  editTask,
  deleteTask,
  startTask
} from "redux/actions/tasks";

import { setTimer, deleteTimer } from "redux/actions/temps";
class Tasks extends React.Component {
  state = {
    task: {}
  };

  componentDidMount = () => {
    console.log("Montado");
  };

  onCreateTask = task => {
    const __task__ = {
      ...task,
      minutes:
        task.minutes >= 120
          ? task.minutes - 120
          : task.minutes >= 60
          ? task.minutes - 60
          : task.minutes,
      hours: task.minutes >= 120 ? 2 : task.minutes >= 60 ? 1 : 0
    };
    this.props.onCreateTask(__task__);
  };

  onStartTask = name => {
    const { tasks, onSetTimer, onDeleteTimer, onStartedTask } = this.props;
    const task = tasks.findIndex(el => el.name === name);
    const {
      onDecrementHours,
      onDecrementMinutes,
      onDecrementSeconds
    } = this.props;
    onStartedTask(name);
    onSetTimer({
      name,
      timer: setInterval(() => {
        const { seconds, minutes, hours, status } = tasks[task];
        if (!["finished"].includes(status)) {
          if (seconds > 0) {
            onDecrementSeconds(name);
          }
          if (seconds === 0) {
            if (minutes === 0) {
              if (hours === 0) {
                clearInterval(this.props.temps[name]);
                this.props.onFinishTask(name);
              } else {
                onDecrementHours(name);
              }
            } else {
              onDecrementMinutes(name);
            }
          }
        } else {
          clearInterval(this.props.temps[name]);
        }
      }, 10)
    });
  };

  onPauseTask = name => {
    clearInterval(this.props.temps[name]);
    this.props.onPauseTask(name);
  };

  onEditTask = task => {
    const __task__ = {
      ...task,
      minutes:
        task.minutes >= 120
          ? task.minutes - 120
          : task.minutes >= 60
          ? task.minutes - 60
          : task.minutes,
      hours: task.minutes >= 120 ? 2 : task.minutes >= 60 ? 1 : 0
    };
    clearInterval(this.props.temps[task.name]);
    this.props.onEditTask(__task__);
  };

  onFinishTask = name => {
    clearInterval(this.props.temps[name]);
    this.props.onFinishTask(name);
  };

  onDeleteTask = (name, index) => {
    console.log(index);
    clearInterval(this.props.temps[name]);
    this.props.onDeleteTask(index);
  };

  render() {
    const {
      onCreateTask,
      onStartTask,
      onPauseTask,
      onFinishTask,
      onEditTask,
      onDeleteTask
    } = this;
    const { tasks } = this.props;

    return (
      <div>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <CreateTask createTask={onCreateTask} />
          </Grid>
          <Grid item xs={12}>
            <Divider />
          </Grid>
          <Grid item xs={12}>
            <TasksList
              tasks={tasks || []}
              startTask={onStartTask}
              pauseTask={onPauseTask}
              finishTask={onFinishTask}
              editTask={onEditTask}
              deleteTask={onDeleteTask}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  init: state.init,
  tasks: state.tasks,
  temps: state.temps
});

const mapDispatchToProps = dispatch => ({
  onDecrementHours: payload => dispatch(decrementHours(payload)),
  onDecrementMinutes: payload => dispatch(decrementMinutes(payload)),
  onDecrementSeconds: payload => dispatch(decrementSeconds(payload)),
  onPauseTask: payload => dispatch(pauseTask(payload)),
  onFinishTask: payload => dispatch(finishTask(payload)),
  onCreateTask: payload => dispatch(createTask(payload)),
  onSetTimer: payload => dispatch(setTimer(payload)),
  onDeleteTimer: payload => dispatch(deleteTimer(payload)),
  onEditTask: payload => dispatch(editTask(payload)),
  onDeleteTask: payload => dispatch(deleteTask(payload)),
  onStartedTask: payload => dispatch(startTask(payload))
});
export default connect(mapStateToProps, mapDispatchToProps)(Tasks);
