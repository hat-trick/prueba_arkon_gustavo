import actionTypes from "constants/actionTypes";

export default function(state = {}, action) {
  switch (action.type) {
    case actionTypes.CREATE_TIMER: {
      const { name, timer } = action.payload;
      state[name] = timer;
      return { ...state };
    }

    case actionTypes.DELETE_TIMER: {
      const { name } = action.payload;
      delete state[name];
      return { ...state };
    }

    default: {
      return { ...state };
    }
  }
}
