import { combineReducers } from "redux";

import init from "redux/reducers/init";
import tasks from "redux/reducers/tasks";
import temps from "redux/reducers/temps";

const rootReducer = combineReducers({
  init,
  tasks,
  temps
});

export default rootReducer;
