import actionTypes from "constants/actionTypes";
import initialState from "constants/initialState";

export default function(state = initialState.init, action) {
  switch (action.type) {
    case actionTypes.INIT: {
      return {
        ...state
      };
    }

    default: {
      return { ...state };
    }
  }
}
