import actionTypes from "constants/actionTypes";
import initialState from "constants/initialState";

export default function(state = initialState.tasks, action) {
  switch (action.type) {
    case actionTypes.DECREMENT_HOURS: {
      const task = state.find(el => el.name === action.payload);
      task.hours = task.hours - 1;
      task.minutes = 59;
      task.seconds = 59;
      task.lapsedSeconds = 0;
      task.lapsedMinutes = task.lapsedMinutes + 1;
      return [...state];
    }
    case actionTypes.DECREMENT_MINUTES: {
      const task = state.find(el => el.name === action.payload);
      if (task.lapsedMinutes === 60 && task.lapsedSeconds === 60) {
        task.lapsedHours += 1;
        task.lapsedMinutes = 0;
      } else {
        task.lapsedMinutes = task.lapsedMinutes + 1;
      }
      task.lapsedSeconds = 0;
      task.minutes = task.minutes - 1;
      task.seconds = 59;

      return [...state];
    }
    case actionTypes.DECREMENT_SECONDS: {
      const task = state.find(el => el.name === action.payload);
      task.seconds = task.seconds - 1;
      task.lapsedSeconds += 1;
      return [...state];
    }
    case actionTypes.PAUSE_TASK: {
      const task = state.find(el => el.name === action.payload);
      task.status = "paused";
      return [...state];
    }
    case actionTypes.FINISH_TASK: {
      const task = state.find(el => el.name === action.payload);
      task.status = "finished";
      return [...state];
    }

    case actionTypes.CREATE_TASK: {
      const task = {
        ...action.payload,
        lapsedHours: 0,
        lapsedMinutes: 0,
        lapsedSeconds: 0,
        status: "created"
      };
      return [...state, task];
    }

    case actionTypes.UPDATE_TASK: {
      const { hours, minutes, seconds, description, name } = action.payload;
      let task = state.find(el => el.name === name);

      task.hours = parseInt(hours, 10);
      task.minutes = parseInt(minutes, 10);
      task.seconds = parseInt(seconds, 10);
      task.description = description;

      return [...state];
    }

    case actionTypes.DELETE_TASK: {
      const index = action.payload;
      state.splice(index, 1);
      return [...state];
    }

    case actionTypes.START_TASK: {
      const task = state.find(el => el.name === action.payload);
      task.status = "started";
      return [...state];
    }
    default: {
      return [...state];
    }
  }
}
