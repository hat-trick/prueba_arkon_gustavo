import actionTypes from "constants/actionTypes";

export const setTimer = payload => ({
  type: actionTypes.CREATE_TIMER,
  payload
});

export const deleteTimer = payload => ({
  type: actionTypes.DELETE_TIMER,
  payload
});
