import actionTypes from "constants/actionTypes";

export const decrementHours = payload => ({
  type: actionTypes.DECREMENT_HOURS,
  payload
});

export const decrementMinutes = payload => ({
  type: actionTypes.DECREMENT_MINUTES,
  payload
});
export const decrementSeconds = payload => ({
  type: actionTypes.DECREMENT_SECONDS,
  payload
});

export const startTask = payload => ({
  type: actionTypes.START_TASK,
  payload
});
export const pauseTask = payload => ({
  type: actionTypes.PAUSE_TASK,
  payload
});

export const finishTask = payload => ({
  type: actionTypes.FINISH_TASK,
  payload
});

export const createTask = payload => ({
  type: actionTypes.CREATE_TASK,
  payload
});

export const editTask = payload => ({
  type: actionTypes.UPDATE_TASK,
  payload
});

export const deleteTask = payload => ({
  type: actionTypes.DELETE_TASK,
  payload
});
