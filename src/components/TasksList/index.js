import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { Button, ButtonGroup } from "@material-ui/core";
import EditTask from "components/EditTask";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  }
}));

export default function CenteredGrid({
  tasks,
  startTask,
  pauseTask,
  finishTask,
  editTask,
  deleteTask
}) {
  const classes = useStyles();

  const buildList = () =>
    tasks.map((el, index) => (
      <Grid container spacing={3}>
        <Grid item xs={2}>
          <Paper className={classes.paper} elevation={0}>
            {el.name}
          </Paper>
        </Grid>
        <Grid item xs={2}>
          <Paper
            elevation={0}
            className={classes.paper}
          >{`${el.hours}: ${el.minutes}: ${el.seconds}`}</Paper>
        </Grid>
        <Grid item xs={2}>
          <Paper
            elevation={0}
            className={classes.paper}
          >{`${el.lapsedHours}: ${el.lapsedMinutes}: ${el.lapsedSeconds}`}</Paper>
        </Grid>
        <Grid item xs={2}>
          <Paper elevation={0} className={classes.paper}>
            {el.description}
          </Paper>
        </Grid>
        <Grid item xs={4}>
          <Paper elevation={0} className={classes.paper}>
            <ButtonGroup size="small" aria-label="small outlined button group">
              {el.status === "paused" || el.status === "created" ? (
                <Button onClick={() => startTask(el.name)}>
                  {el.status === "paused" ? "Reanudar" : "Iniciar"}
                </Button>
              ) : (
                ""
              )}
              {el.status === "started" && (
                <Button onClick={() => pauseTask(el.name)}>Pausar</Button>
              )}
              {el.status !== "finished" && (
                <Button onClick={() => finishTask(el.name)}>Finalizar</Button>
              )}
              <Button onClick={() => deleteTask(el.name, index)}>
                Eliminar
              </Button>
              <EditTask task={el} editTask={editTask} />
            </ButtonGroup>
          </Paper>
        </Grid>
      </Grid>
    ));

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={2}>
          <Paper className={classes.paper} elevation={0}>
            <span style={{ fontWeight: "bold" }}>Nombre</span>
          </Paper>
        </Grid>
        <Grid item xs={2}>
          <Paper elevation={0} className={classes.paper}>
            <span style={{ fontWeight: "bold" }}>Duración</span>
          </Paper>
        </Grid>
        <Grid item xs={2}>
          <Paper elevation={0} className={classes.paper}>
            <span style={{ fontWeight: "bold" }}>Transcurrido</span>
          </Paper>
        </Grid>
        <Grid item xs={2}>
          <Paper elevation={0} className={classes.paper}>
            <span style={{ fontWeight: "bold" }}>Descripción </span>
          </Paper>
        </Grid>
        <Grid item xs={4}>
          <Paper elevation={0} className={classes.paper}>
            <span style={{ fontWeight: "bold" }}>Acciones</span>
          </Paper>
        </Grid>
      </Grid>
      {buildList()}
    </div>
  );
}
