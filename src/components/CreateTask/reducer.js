export default (state, action) => {
  switch (action.type) {
    case "ON_CHANGE": {
      const { name, value } = action.payload;
      return {
        ...state,
        task: {
          ...state.task,
          [name]: value
        }
      };
    }
    case "SHOULD_EDIT_TIME_ENABLED": {
      return {
        ...state,
        shouldEditTime: true
      };
    }
    case "SHOULD_EDIT_TIME_DISABLED": {
      return {
        ...state,
        shouldEditTime: false
      };
    }
    case "SET_TIME_BY_SELECT": {
      return {
        ...state,
        task: {
          ...state.task,
          minutes: action.payload,
          seconds: 0
        }
      };
    }
    case "SET_VALIDATE": {
      return {
        ...state,
        validate: action.payload
      };
    }
    default:
      throw new Error();
  }
};
