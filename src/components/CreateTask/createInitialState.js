export default (task = {}) => ({
  editable: task ? true : false,
  shouldEditTime: false,
  task: {
    name: task.name || "",
    hours: task.hours || 0,
    minutes: task.hours || 0,
    description: task.description || "",
    seconds: task.seconds || 0,
    finished: false,
    status: task.status || "created",
    lapsedHours: task.lapsedHours || 0,
    lapsedMinutes: task.lapsedMinutes || 0,
    lapsedSeconds: task.lapsedSeconds || 0
  }
});
