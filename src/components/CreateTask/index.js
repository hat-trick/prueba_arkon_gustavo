import React, { useReducer } from "react";
import {
  Button,
  TextField,
  Grid,
  Select,
  MenuItem,
  InputLabel,
  FormControl
} from "@material-ui/core";
import reducer from "components/CreateTask/reducer";
import useStyles from "components/CreateTask/useStyles";
import createInitialState from "components/CreateTask/createInitialState";

export default ({ createTask, task, editTask }) => {
  const classes = useStyles();
  const initialState = createInitialState(task);
  const [state, dispatch] = useReducer(reducer, initialState);
  const [time, setTime] = React.useState("");
  const inputLabel = React.useRef(null);

  const onChangeItemValue = e => {
    const { name, value } = e.target;
    dispatch({ type: "ON_CHANGE", payload: { name, value } });
  };

  const handleSelectTime = ({ target }) => {
    const { value } = target;
    if (value === 4) dispatch({ type: "SHOULD_EDIT_TIME_ENABLED" });
    else {
      dispatch({ type: "SET_TIME_BY_SELECT", payload: value });
      dispatch({ type: "SHOULD_EDIT_TIME_DISABLED" });
    }
    setTime(value);
  };

  const onCreateTask = () => {
    createTask(state.task);
  };

  const onEditTask = () => {
    editTask(state.task);
  };

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={3}>
          <TextField
            disabled={state.editable}
            id="name"
            name="name"
            helperText="Nombre"
            color="secondary"
            value={state.task && state.task.name}
            onChange={onChangeItemValue}
          />
        </Grid>
        <Grid item xs={3}>
          <FormControl className={classes.formControl}>
            <InputLabel ref={inputLabel} id="time">
              Duración
            </InputLabel>
            <Select
              labelId="time"
              id="time"
              value={time}
              onChange={handleSelectTime}
            >
              <MenuItem value={30}>30 mins</MenuItem>
              <MenuItem value={45}>45 mins</MenuItem>
              <MenuItem value={60}>1 h</MenuItem>
              <MenuItem value={4}>Ingresar otro</MenuItem>
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={3}>
          <TextField
            disabled={!state.shouldEditTime}
            id="minutes"
            name="minutes"
            helperText="Minutos"
            color="secondary"
            value={state.task && state.task.minutes}
            onChange={onChangeItemValue}
          />
        </Grid>
        <Grid item xs={3}>
          <TextField
            disabled={!state.shouldEditTime}
            id="seconds"
            name="seconds"
            helperText="Segundos"
            color="secondary"
            value={state.task && state.task.seconds}
            onChange={onChangeItemValue}
          />
        </Grid>
        <Grid item xs={3}>
          <TextField
            id="description"
            name="description"
            helperText="Descripción"
            color="secondary"
            value={state.task && state.task.description}
            onChange={onChangeItemValue}
          />
        </Grid>
        <Grid item xs={2}>
          {state.editable && (
            <Button color="primary" onClick={onEditTask}>
              Editar
            </Button>
          )}
          {!state.editable && (
            <Button color="primary" variant="outlined" onClick={onCreateTask}>
              Crear Tarea
            </Button>
          )}
        </Grid>
      </Grid>
    </div>
  );
};
