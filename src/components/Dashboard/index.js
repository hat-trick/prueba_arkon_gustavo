import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  }
}));

export default function CenteredGrid({ tasks }) {
  const classes = useStyles();

  const filteredTasks = tasks.filter(el => el.status === "finished");

  const buildList = () =>
    filteredTasks.map((el, index) => (
      <Grid container spacing={3}>
        <Grid item xs={2}>
          <Paper className={classes.paper} elevation={0}>
            {el.name}
          </Paper>
        </Grid>
        <Grid item xs={2}>
          <Paper
            elevation={0}
            className={classes.paper}
          >{`${el.hours}: ${el.minutes}: ${el.seconds}`}</Paper>
        </Grid>
        <Grid item xs={2}>
          <Paper className={classes.paper} elevation={0}>
            {el.status}
          </Paper>
        </Grid>
        <Grid item xs={2}>
          <Paper
            elevation={0}
            className={classes.paper}
          >{`${el.lapsedHours}: ${el.lapsedMinutes}: ${el.lapsedSeconds}`}</Paper>
        </Grid>
        <Grid item xs={2}>
          <Paper elevation={0} className={classes.paper}>
            {el.description}
          </Paper>
        </Grid>
      </Grid>
    ));

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={2}>
          <Paper className={classes.paper} elevation={0}>
            <span style={{ fontWeight: "bold" }}>Nombre</span>
          </Paper>
        </Grid>
        <Grid item xs={2}>
          <Paper elevation={0} className={classes.paper}>
            <span style={{ fontWeight: "bold" }}>Duración</span>
          </Paper>
        </Grid>
        <Grid item xs={2}>
          <Paper elevation={0} className={classes.paper}>
            <span style={{ fontWeight: "bold" }}>Estatus</span>
          </Paper>
        </Grid>
        <Grid item xs={2}>
          <Paper elevation={0} className={classes.paper}>
            <span style={{ fontWeight: "bold" }}>Transcurrido</span>
          </Paper>
        </Grid>
        <Grid item xs={2}>
          <Paper elevation={0} className={classes.paper}>
            <span style={{ fontWeight: "bold" }}>Descripción </span>
          </Paper>
        </Grid>
      </Grid>
      {buildList()}
    </div>
  );
}
